class AddUserAccountsAndNewHplcModels < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :username, null: false
      t.string :email, null: false
      t.string :password_digest, null: false
    end
    add_index :users, :username, unique: true

    create_table :processing_requests do |t|
      t.integer :user_id, null: :false
      t.string  :original_file_path
      t.string  :processed_file_path
      t.integer :rows_to_process
      t.float   :percentage_processed, default: 0.0
    end
    add_index :processing_requests, :user_id
  end
end
