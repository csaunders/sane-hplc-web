class AddNameToProcessingRequest < ActiveRecord::Migration[5.1]
  def change
    change_table :processing_requests do |t|
      t.string :name, null: false
    end
    add_index :processing_requests, [:user_id, :name], unique: true
  end
end
