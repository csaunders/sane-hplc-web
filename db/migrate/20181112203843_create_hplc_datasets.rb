class CreateHplcDatasets < ActiveRecord::Migration[5.1]
  def change
    create_table :hplc_datasets do |t|
      t.string :filename
      t.text :encoded_zip
      t.text :processed_data

      t.timestamps
    end
  end
end
