# Sane HPLC Web

A rudimentary Rails application for easily performing tranformations to
HPLC output from the GvdM equipment.

## Deploying & Rebooting

```
ssh grohl@hplc.escarpmentlabs.com
cd apps/sane-hplc-web
# If you have any code changes that need to be deployed
git pull origin master
./deploy.sh
```

Once the application is up and running and you can [access the application](https://hplc.escarpmentlabs.com)
you can stop viewing the logs by pressing `Ctrl+C`
