require 'yaml'

class KnowledgeBase
  KNOWLEDGE_BASE_FILE_LOCATION = File.join(File.dirname(__FILE__), 'knowledge_base.yaml')
  attr_reader :kb
  def initialize
    @kb = YAML.load(File.read(KNOWLEDGE_BASE_FILE_LOCATION)).map do |key, values|
      KBEntry.new(key, values)
    end.sort
  end

  def resolve(datapoints)
    iterator = @kb.to_enum
    kb_entry = iterator.next
    datapoints.map do |datapoint|
      kb_entry = evaluate(datapoint, kb_entry, iterator) unless kb_entry.nil?
      if kb_entry && kb_entry.candidate?(datapoint.rtime)
        datapoint.metabolite = kb_entry.name 
        datapoint.predicted_conc = kb_entry.calculate_concentration(datapoint.height)
      end
      datapoint 
    end
  end

  private
  def evaluate(datapoint, kb_entry, kb_iterator)
    if !kb_entry.candidate?(datapoint.rtime) && datapoint.rtime > kb_entry.rtime
      kb_entry = kb_iterator.next
      evaluate(datapoint, kb_entry, kb_iterator)
    end
    kb_entry
  rescue StopIteration
    return
  end
end