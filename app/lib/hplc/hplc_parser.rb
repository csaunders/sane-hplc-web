require 'csv'

class HPLCParser
  class HPLCEntry
    attr_reader :area, :height, :rtime, :peak
    attr_accessor :metabolite, :filename, :predicted_conc
    def initialize(row, filename)
      @peak = row['Peak#'].to_i
      @area = row['Area'].to_f
      @height = row['Height'].to_f
      @rtime = (row['R.Time'].to_f * 60).to_i # converts minutes into seconds -- more granularity; easier math
      @metabolite = row['Name']
      @filename = filename
    end

    def to_s
      [peak, rtime, metabolite, area, height].join(',')
    end
  end

  DATA_START_TAG = "[Peak Table(Detector A)]"
  DATA_HEADER_PREFIX = "Peak#\tR.Time"
  CSV_SEPARATOR = "\t"
  def parse(io, filename: nil)
    data = load_data(io)
    CSV.new(data, col_sep: CSV_SEPARATOR, headers: true).map { |row| HPLCEntry.new(row, filename) }
  end

  private
  def load_data(input_stream)
    hit_start_tag = false
    extracting_data = false
    data = StringIO.new
    input_stream.each do |line|
      hit_start_tag = true if line.strip == DATA_START_TAG
      extracting_data = true if line.include?(DATA_HEADER_PREFIX)
      next unless hit_start_tag && extracting_data
      break if line.strip.size == 0
      data << line
    end
    data.rewind
    data
  end
end