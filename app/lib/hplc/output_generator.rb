class OutputGenerator
  HEADER = %w(Filename Peak# R.Time Metabolite Area Height Predicted-Conc).join(',')
  def initialize
    @results = [HEADER]
  end

  def headers
    HEADER
  end

  def row_for(dp)
    [
      dp.filename,
      dp.peak,
      dp.rtime,
      dp.metabolite,
      dp.area,
      dp.height,
      dp.predicted_conc,      
    ].join(',')
  end

  def add(dp)
    @results << [
      dp.filename,
      dp.peak,
      dp.rtime,
      dp.metabolite,
      dp.area,
      dp.height,
      dp.predicted_conc
    ].join(',')
  end

  def to_csv
    @results.join("\n")
  end
end