class KBEntry
  attr_reader :name, :rtime, :tolerance, :intercept, :slope
  def initialize(name, values)
    @name = name
    @rtime = values['resolution_time']
    @tolerance = values['tolerance'] || 30
    @intercept = values['intercept'] || 1
    @slope = values['slope'] || 1
  end

  def candidate?(time_value)
    time_value >= lower_limit && time_value <= upper_limit
  end

  def upper_limit
    rtime + tolerance
  end

  def lower_limit
    rtime - tolerance
  end

  def calculate_concentration(height)
    (height - intercept) / slope
  end

  def <=>(other)
    self.rtime <=> other.rtime
  end
end