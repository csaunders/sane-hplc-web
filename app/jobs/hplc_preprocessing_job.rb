class HplcPreprocessingJob < ApplicationJob
  queue_as :normal

  class EntryData
    attr_accessor :name, :num_rows
    def initialize(name, io)
      @name = name
      @io = io
    end

    def io
      @io.rewind; @io
    end
  end

  def perform(**args)
    processing_request = ProcessingRequest.find(args[:id])

    parser = HPLCParser.new
    zip = Zip::File.open(processing_request.original_file_path)
    datapoints = collect_entry_data(zip).map do |entry|
      parser.parse(entry.io, filename: entry.name).tap {
        entry.io.close
      }
    end.flatten

    HplcProcessingJob.new.perform(id: args[:id], datapoints: datapoints)
  end

  private
  def collect_entry_data(zip)
    zip.entries.map do |entry|
      next if entry.name[0...2] == "__"
      EntryData.new(entry.name, zip.get_input_stream(entry))
    end.reject(&:nil?).flatten
  end
end