require 'hplc'
require 'zip'

class HplcProcessingJob < ApplicationJob
  queue_as :normal

  class ProcessingRequestUpdater
    attr_accessor :processing_request
    def initialize(proc_req)
      @total_rows_processed = 0
      @processing_request = proc_req
    end

    def update_total_rows(total_rows)
      processing_request.update_attributes(rows_to_process: total_rows)
    end

    def update_progress(lines_processed)
      @total_rows_processed += lines_processed
      processing_request.update_attributes(percentage_processed: @total_rows_processed.to_f / processing_request.rows_to_process)
    end
  end

  def perform(**args)
    processing_request = ProcessingRequest.find(args[:id])
    datapoints = args[:datapoints]
    processing_request.prepare_output_file!

    updater = ProcessingRequestUpdater.new(processing_request)
    updater.update_total_rows(datapoints.size)

    knowledge_base = KnowledgeBase.new
    knowledge_base.resolve(datapoints)

    generator = OutputGenerator.new
    processing_request.output_file_handle do |f|
      datapoints.each_with_index do |datapoint, index|
        f.puts generator.headers if index == 0
        f.puts generator.row_for(datapoint)
        updater.update_progress(1)
      end
      f.flush
    end
  end
end