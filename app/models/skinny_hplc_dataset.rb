class SkinnyHplcDataset
  SKINNY_HPLC_QUERY = <<-QUERY
    select
      id, filename,
      length(encoded_zip) as original_file_size,
      length(processed_data) as processed_file_size
    from 
      hplc_datasets
  QUERY

  def self.records
    records = ActiveRecord::Base.connection.execute(SKINNY_HPLC_QUERY).values
    records.map do |(id, filename, original_bytes, processed_bytes)|
      SkinnyHplcDataset.new(id, filename, original_bytes, processed_bytes)
    end
  end

  attr_reader :id, :filename, :original_file_size, :processed_file_size
  def initialize(id, filename, original_file_size, processed_file_size)
    @id = id
    @filename = filename
    @original_file_size = original_file_size
    @processed_file_size = processed_file_size
  end
end