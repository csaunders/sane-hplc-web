require 'hplc'
require 'zip'

class HplcDataset < ApplicationRecord

  def zip_file
    io = StringIO.new
    io << Base64.decode64(encoded_zip)
    io.rewind
    Zip::File.open_buffer(io)
  end

  def process!
    parser = HPLCParser.new
    kb = KnowledgeBase.new
    generator = OutputGenerator.new
    zf = zip_file
    zf.entries.each do |entry|
      next if entry.name[0...2] == "__"
      io = zf.get_input_stream(entry)
      datapoints = parser.parse(io, filename: entry.name)
      kb.resolve(datapoints)
      datapoints.each { |dp| generator.add(dp) }
    end

    update_attributes(processed_data: generator.to_csv)
  end

  def self.from_zip(filename, io)
    new(filename: filename, encoded_zip: Base64.encode64(io.read))
  end
end
