class User < ApplicationRecord
  before_save :downcase_attributes
  validates :email, :username, presence: true
  has_secure_password
  has_many :processing_requests

  private
  def downcase_attributes
    self.username = username.downcase
    self.email = email.downcase
  end
end