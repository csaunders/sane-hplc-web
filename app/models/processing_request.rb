class ProcessingRequest < ApplicationRecord
  INPUT_FOLDER = Rails.root.join('public/processing_requests/input')
  OUTPUT_FOLDER = Rails.root.join('public/processing_requests/output')
  belongs_to :user
  validate :validates_input_and_output_file_paths, on: :save
  before_destroy { |record|
    File.delete(original_file_path) if original_file_path.present? && File.exists?(original_file_path)
    File.delete(processed_file_path) if processed_file_path.present? && File.exists?(processed_file_path)
  }

  def write_input_file_to_disk!(tempfile)
    tempfile.rewind
    self.original_file_path = File.join(INPUT_FOLDER, "#{self.id}.zip")
    File.open(original_file_path, "wb") do |f|
      f << tempfile.read
      f.flush
    end
    save!
  end

  def prepare_output_file!
    self.processed_file_path ||= File.join(OUTPUT_FOLDER, "#{self.id}.csv")
    # Clear out file contents
    File.open(processed_file_path, "wb") { |f| f.flush }
    save!
  end

  def output_file_handle(&blk)
    File.open(processed_file_path, "ab") do |f|
      yield(f)
    end
  end

  def write_output_to_disk(results)
    self.processed_file_path = File.join(OUTPUT_FOLDER, "#{self.id}.csv")
    File.open(processed_file_path, "wb") do |f|
      f << results
      f.flush
    end
    save!
  end

  def processed?
    processed_file_path.present? && File.exists?(processed_file_path) && percentage_processed >= 1
  end

  private
  def validates_input_and_output_file_paths
    if original_file_path.present? && !original_file_path.start_with?(INPUT_FOLDER)
      errors.add(:original_file_path, "path is not valid")
    end

    if processed_file_path.present? && !processed_file_path.start_with(OUTPUT_FOLDER)
      errors.add(:processed_file_path, "path is not valid")
    end
  end
end