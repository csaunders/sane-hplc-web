class DatasetsController < AuthenticatedAccountsController
  helper_method :processing_requests
  before_action :verify_file_type, only: %i(create)
  def index
  end

  def show
    csv = File.read processing_request.processed_file_path
    send_data csv, filename: "#{processing_request.name}.processed.csv"
  end

  def create
    zip = processing_request_params[:zipfile]
    processing_request = current_user.processing_requests.create(name: processing_request_params[:name])
    begin
      processing_request.write_input_file_to_disk!(zip.tempfile)
      HplcPreprocessingJob.perform_later(id: processing_request.id)
    rescue => exception
      processing_request.destroy!
      flash[:error] = "The processing request could not be uploaded to the server"
      raise exception
    end
    redirect_to datasets_path
  end

  private
  def processing_requests
    @processing_requests ||= current_user.processing_requests
  end

  def processing_request
    @processing_request ||= current_user.processing_requests.find(params[:id])
  end

  def processing_request_params
    params.require(:processing_request).permit(:name, :zipfile)
  end

  def verify_file_type
    extension = File.extname(processing_request_params[:zipfile].original_filename)
    return if extension == ".zip"
    flash[:error] = "Job requests must be submitted as compressed ZIP files"
    redirect_to datasets_path
  end
end