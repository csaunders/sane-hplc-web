class LegacyDatasetsController < AuthenticatedAccountsController
  def index
    @datasets = SkinnyHplcDataset.records
    @dataset = HplcDataset.new
  end

  def show
    dataset = HplcDataset.find(params[:id])
    send_data(dataset.processed_data, :type => 'text/csv; charset=utf-8; header=present', :filename => dataset.filename.gsub('.zip', '.csv'))
  end

  def create
    zip = dataset_params[:zipfile]
    dataset = HplcDataset.from_zip(zip.original_filename, zip.tempfile)
    dataset.save!
    dataset.process!
    redirect_to datasets_path
  end

  private
  def dataset_params
    params.require(:hplc_dataset).permit(:zipfile)
  end
end