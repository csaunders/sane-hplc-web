class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :user_logged_in?
  
  protected

  def user_logged_in?
    session[:user_id].present? && current_user.present?
  end

  def log_in(user)
    session[:user_id] = user.id
  end

  def invalid_authentication_error
    flash[:notice] = "Invalid Credentials"
    redirect_to root_path
  end

  def current_user
    @current_user ||= User.where(id: session[:user_id]).first
  end
end
