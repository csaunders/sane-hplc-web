class AuthenticatedAccountsController < ApplicationController
  before_action :ensure_user_logged_in

  private
  def ensure_user_logged_in
    redirect_to root_path unless user_logged_in?
  end
end