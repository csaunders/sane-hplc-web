class AuthenticationsController < ApplicationController
  helper_method :user
  before_action :redirect_if_logged_in, except: :logout
  def index
  end

  def create
    user = User.where(username: create_params[:username], email: create_params[:email]).first_or_initialize
    invalid_authentication_error and return unless user.id.blank?
    user.update_attributes(create_params)
    if user.save
      log_in(user)
      redirect_to datasets_path and return
    end
    redirect_to root_path
  end

  def login
    user = User.where(username: login_params[:username]).first
    invalid_authentication_error and return unless user && user.authenticate(login_params[:password])
    log_in(user)
    redirect_to datasets_path
  end

  def logout
    reset_session if user_logged_in?
    flash[:notice] = "Logged Out"
    redirect_to root_path
  end

  private
  def user
    @user ||= User.new
  end

  def redirect_if_logged_in
    redirect_to datasets_path if user_logged_in?
  end

  def login_params
    @login_params ||= params.require(:user).permit(:username, :password)
  end

  def create_params
    @create_params ||= params.require(:user).permit(:username, :email, :password, :password_confirmation)
  end
end