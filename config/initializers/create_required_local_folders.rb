[ProcessingRequest::INPUT_FOLDER, ProcessingRequest::OUTPUT_FOLDER].each do |path|
  FileUtils.mkdir_p(path) unless File.directory?(path)
end