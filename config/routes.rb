Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope :legacy do
    resources :datasets, only: %i(index show), controller: 'legacy_datasets', as: 'legacy_datasets'
  end

  resources :datasets, only: %i(index show create)
  resources :authentications, only: %i(index create) do
    post 'login', on: :collection
    delete 'logout', on: :collection
  end  
  root to: 'authentications#index'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/jobs', constraints: LoggedInConstraint.new
end
