FROM ruby:2.5
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - \
   && apt-get update -qq && apt-get install -y nodejs postgresql-client
# RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
# RUN apt-get update -qq && apt-get install -y postgresql-client

WORKDIR /app

COPY . /app

RUN RAILS_ENV=production bundle install --deployment --without development test
# TODO: Figure out how to get assets compiling without secrets
# RUN RAILS_ENV=production bundle exec rake assets:precompile

EXPOSE 3000